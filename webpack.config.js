const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  "mode": "production",
  "bail": true,
  "devtool": false,
  "entry": __dirname + "/src/app.js",
  "output": {
    path: path.resolve(__dirname, 'dist'),
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        "include": [
          __dirname + "/src"
        ],
        "exclude": [
          __dirname + "/node_modules"
        ],
        use: [
          "css-loader",
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: [
                  [
                    "postcss-preset-env",
                    {
                      // Options
                    },
                  ],
                ],
              },
            },
          },
        ],
      },
      {
        "test": /\.css$/,
        // "include": [
        //   __dirname + "/node_modules"
        // ],
        "exclude": [
          __dirname + "/src"
        ],
        "use": [
          MiniCssExtractPlugin.loader,
          {
            "loader": "css-loader",
            "options": {
              "sourceMap": false
            }
          }
        ],
        "sideEffects": true
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      // filename: 'style.css',
    }),
  ]
}
